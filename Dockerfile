FROM openjdk:8-jre-alpine
ENV LANG=C.UTF-8 TZ=Asia/Shanghai
RUN apk add --no-cache --update tzdata \
&& rm -rf /tmp/* /var/cache/apk/* /var/lib/apk/lists/* \
CMD ["/bin/sh"]
